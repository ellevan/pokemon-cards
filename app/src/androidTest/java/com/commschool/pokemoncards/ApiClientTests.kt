package com.commschool.pokemoncards

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.commschool.pokemoncards.data.network.NetworkClient
import com.commschool.pokemoncards.net.UserSession
import com.commschool.pokemoncards.net.ApiClient
import kotlinx.coroutines.runBlocking

import org.junit.Test
import org.junit.runner.RunWith

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ApiClientTests {
    @Test
    fun testLogin() {
        ApiClient
            .userService
            .login(username = "test", password = "testa")
            .enqueue(object : Callback<UserSession> {
                override fun onResponse(call: Call<UserSession>, response: Response<UserSession>) {
                    TODO("Not yet implemented")
                }

                override fun onFailure(call: Call<UserSession>, t: Throwable) {
                    TODO("Not yet implemented")
                }

            })

        Thread.sleep(2000)
    }

    @Test
    fun testNameQuery() = runBlocking {

        val bulbo = NetworkClient
            .pokemonService
            .getCardByName("bulbo")

        assert(bulbo.data.isNotEmpty())

    }
}
