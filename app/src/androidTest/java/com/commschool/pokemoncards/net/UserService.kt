package com.commschool.pokemoncards.net

import com.commschool.pokemoncards.net.UserSession
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface UserService {

    @POST("auth/login")
    fun login(
        @Query("username") username: String,
        @Query("password") password: String
    ): Call<com.commschool.pokemoncards.net.UserSession>


}