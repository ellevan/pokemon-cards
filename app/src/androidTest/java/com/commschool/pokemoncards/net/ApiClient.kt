package com.commschool.pokemoncards.net

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


//def retrofit_version = "2.9.0"
//implementation "com.squareup.retrofit2:retrofit:$retrofit_version"
//implementation "com.squareup.retrofit2:converter-moshi:$retrofit_version"
//def moshi_version = "1.11.0"
//implementation "com.squareup.moshi:moshi:$moshi_version"
//implementation "com.squareup.moshi:moshi-kotlin:$moshi_version"
//implementation "com.squareup.okhttp3:logging-interceptor:4.9.0"
//

object ApiClient {

    val userService by lazy { createUserService() }

    private fun createUserService(): UserService {

        val retrofitBuilder = Retrofit.Builder()
        retrofitBuilder.baseUrl("https://commschool-android-api.herokuapp.com/")
        retrofitBuilder.client(
            OkHttpClient().newBuilder()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }).build()
        )
        retrofitBuilder.addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder()
                    .addLast(KotlinJsonAdapterFactory())
                    .build()
            )
        )
        return retrofitBuilder.build().create(UserService::class.java)
    }

}