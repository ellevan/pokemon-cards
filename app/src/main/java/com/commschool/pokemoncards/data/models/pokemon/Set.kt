package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import androidx.room.Embedded
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Set(
    @Json(name = "id")
    val id: String,
    @Json(name = "images")
    @Embedded
    val images: SetImages,
    @Json(name = "name")
    val name: String,
    @Json(name = "printedTotal")
    val printedTotal: Int,
    @Json(name = "releaseDate")
    val releaseDate: String,
    @Json(name = "series")
    val series: String,
    @Json(name = "total")
    val total: Int,
) : Parcelable