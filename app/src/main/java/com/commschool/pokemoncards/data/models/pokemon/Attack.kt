package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Attack(
    @Json(name = "convertedEnergyCost")
    val convertedEnergyCost: Int,
    @Json(name = "cost")
    val cost: List<String>,
    @Json(name = "damage")
    val damage: String?,
    @Json(name = "name")
    val name: String,
    @Json(name = "text")
    val text: String
):Parcelable