package com.commschool.pokemoncards.data.repository

import android.service.autofill.Dataset
import com.commschool.pokemoncards.data.models.pokemon.PaginatedData
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import com.commschool.pokemoncards.data.models.user.UserRegistrationRequest
import com.commschool.pokemoncards.data.network.NetworkClient
import com.commschool.pokemoncards.data.storage.DataStore
import com.commschool.pokemoncards.data.storage.db.entities.SavedCardIdEntity
import kotlinx.coroutines.flow.map
import java.io.DataInputStream

object Repository {

    suspend fun getLocalCardById(id: String): PokeCard? {
        return DataStore.db.getCardDAO().getById(id)
    }

    suspend fun getRemoteCardById(id: String): PokeCard {
        return NetworkClient.pokemonService.getCardBydId(id).data
            .also {
                DataStore.db.getCardDAO().insert(it)
            }
    }

    suspend fun getRemoteCardsCardsAndStore(
        page: Int,
        pageSize: Int
    ): PaginatedData<List<PokeCard>> {
        val cards = NetworkClient.pokemonService.getCards(
            page = page,
            pageSize = pageSize
        )
        DataStore.db.getCardDAO().insert(cards.data)
        return cards
    }

    fun getLocalSavedCardsFlow() =
        DataStore.db.getSavedCardsDao().getSavedCardFlow()
            .map { list -> list.map { it.id } }

    suspend fun clearSavedCards() =
        DataStore.db.getSavedCardsDao().deleteAll()


    suspend fun updateRemoteSavedCards(): List<String> {
        val ids = NetworkClient.userService.getUserCards().map { it }
        DataStore.db.getSavedCardsDao().insert(ids = ids.map { SavedCardIdEntity(it) })
        DataStore.lastTimeSavedCardsFetched = System.currentTimeMillis()
        return ids
    }

    suspend fun loginAndSetToken(username: String, password: String) {
        NetworkClient.userService.login(
            username = username,
            password = password
        ).apply {
            DataStore.authToken = accessToken
        }
    }

    suspend fun registerAndLogin(
        name: String,
        userName: String,
        password: String
    ) {
        NetworkClient.userService.register(
            UserRegistrationRequest(
                name = name,
                userName = userName,
                password = password
            )
        )
        NetworkClient.userService.login(
            username = userName,
            password = password
        ).accessToken.apply {
            DataStore.authToken = this
        }
    }

    suspend fun saveCard(card: PokeCard) {
        NetworkClient.userService.saveUserCards(card.id)
        DataStore.db.getSavedCardsDao().insert(SavedCardIdEntity(card.id))
    }

    suspend fun deleteCard(card: PokeCard) {
        NetworkClient.userService.deleteUserCard(card.id)
        DataStore.db.getSavedCardsDao().delete(SavedCardIdEntity(card.id))
    }

    suspend fun checkSavedIdsValidity(): Boolean =
        System.currentTimeMillis() - DataStore.lastTimeSavedCardsFetched < 60 * 60 * 1000

    suspend fun invalidateSavedIds() {
        DataStore.lastTimeSavedCardsFetched = 0
    }

    suspend fun getRemoteAndSaveProfile() {
        DataStore.db.getUserProfileDAO().insert(
            NetworkClient.userService.getUser()
        )
    }

    suspend fun clearProfile() {
        DataStore.db.getUserProfileDAO().delete()
    }

    suspend fun getCardsByName(string: String) = NetworkClient.pokemonService.getCards(
        "name:$string*"
    ).data

    suspend fun getCardsBySet(setId: String) =
        NetworkClient.pokemonService.getCards(
            query = "set.id:$setId"
        ).data.also {
            DataStore.db.getCardDAO().insert(it)
        }

}