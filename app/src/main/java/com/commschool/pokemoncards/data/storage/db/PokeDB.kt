package com.commschool.pokemoncards.data.storage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import com.commschool.pokemoncards.data.models.user.UserProfile
import com.commschool.pokemoncards.data.storage.db.entities.SavedCardIdEntity
import com.commschool.pokemoncards.data.storage.db.typeConvertors.*

@Database(entities = [PokeCard::class, SavedCardIdEntity::class, UserProfile::class], version = 1)
@TypeConverters(
    StringListTypeConverter::class,
    AbilityTypeConverter::class,
    AttackTypeConverter::class,
    ResistanceTypeConverter::class,
    WeaknesseTypeConverter::class
)
abstract class PokeDB : RoomDatabase() {
    abstract fun getCardDAO(): CardDao
    abstract fun getSavedCardsDao(): SavedCardIdDao
    abstract fun getUserProfileDAO(): UserProfileDao
}