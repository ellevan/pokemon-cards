package com.commschool.pokemoncards.data.network

import com.commschool.pokemoncards.data.models.user.UserProfile
import com.commschool.pokemoncards.data.models.user.UserRegistrationRequest
import com.commschool.pokemoncards.data.models.user.UserSession
import retrofit2.Response
import retrofit2.http.*

interface UserService {

    @POST("/auth/login")
    suspend fun login(
        @Query("username") username: String,
        @Query("password") password: String
    ): UserSession

    @POST("/auth/register")
    suspend fun register(@Body request: UserRegistrationRequest)

    @GET("/auth/user")
    suspend fun getUser(): UserProfile


    @GET("/user/pokemon/get-my-cards")
    suspend fun getUserCards(): List<String>

    @POST("/user/pokemon/save-card")
    suspend fun saveUserCards(@Query("cardId") cardId: String)

    @DELETE("/user/pokemon/delete-card")
    suspend fun deleteUserCard(@Query("cardId") cardId: String)


}