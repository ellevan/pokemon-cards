package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
@Entity
data class PokeCard(
    @Json(name = "id")
    @PrimaryKey
    val id: String,
    @Json(name = "abilities")
    val abilities: List<Ability> = emptyList(),
    @Json(name = "attacks")
    val attacks: List<Attack> = emptyList(),
    @Json(name = "evolvesFrom")
    val evolvesFrom: String?,
    @Json(name = "hp")
    val hp: String = "0",
    @Json(name = "images")
    @Embedded
    val images: Images,
    @Json(name = "level")
    val level: String?,
    @Json(name = "name")
    val name: String,
    @Json(name = "number")
    val number: String? = null,
    @Json(name = "rarity")
    val rarity: String?,
    @Json(name = "resistances")
    val resistances: List<Resistance> = emptyList(),
    @Json(name = "set")
    @Embedded(prefix = "set_")
    val set: Set,
    @Json(name = "subtypes")
    val subtypes: List<String> = emptyList(),
    @Json(name = "supertype")
    val supertype: String? = null,
    @Json(name = "types")
    val types: List<String> = emptyList(),
    @Json(name = "weaknesses")
    val weaknesses: List<Weaknesse> = emptyList()
) : Parcelable