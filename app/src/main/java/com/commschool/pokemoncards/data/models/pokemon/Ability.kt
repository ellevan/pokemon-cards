package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Ability(
    @Json(name = "name")
    val name: String,
    @Json(name = "text")
    val text: String,
    @Json(name = "type")
    val type: String
) : Parcelable