package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class SetImages(
    @Json(name = "logo")
    val logo: String,
    @Json(name = "symbol")
    val symbol: String
): Parcelable