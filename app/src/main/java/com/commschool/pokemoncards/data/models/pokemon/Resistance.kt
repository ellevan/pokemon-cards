package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Resistance(
    @Json(name = "type")
    val type: String,
    @Json(name = "value")
    val value: String
):Parcelable