package com.commschool.pokemoncards.data.models.pokemon


import com.squareup.moshi.Json
import androidx.annotation.Keep

@Keep
data class PaginatedData<T>(
    @Json(name = "data")
    val data: T,
    @Json(name = "count")
    val count: Int,
    @Json(name = "page")
    val page: Int,
    @Json(name = "pageSize")
    val pageSize: Int,
    @Json(name = "totalCount")
    val totalCount: Int
)