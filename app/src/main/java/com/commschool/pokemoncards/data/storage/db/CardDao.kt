package com.commschool.pokemoncards.data.storage.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.commschool.pokemoncards.data.models.pokemon.PokeCard

@Dao
interface CardDao {

    @Query("select * from pokecard")
    suspend fun getAll(): List<PokeCard>

    @Query("select * from pokecard where id=:id")
    suspend fun getById(id: String): PokeCard?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(pokeCard: PokeCard)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(pokeCard: List<PokeCard>)

}