package com.commschool.pokemoncards.data.models.pokemon


import android.os.Parcelable
import com.squareup.moshi.Json
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Images(
    @Json(name = "large")
    val large: String,
    @Json(name = "small")
    val small: String
):Parcelable