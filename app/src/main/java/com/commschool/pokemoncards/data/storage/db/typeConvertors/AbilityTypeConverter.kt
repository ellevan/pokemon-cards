package com.commschool.pokemoncards.data.storage.db.typeConvertors

import androidx.room.TypeConverter
import com.commschool.pokemoncards.data.models.pokemon.Ability
import com.commschool.pokemoncards.data.models.pokemon.Attack
import com.commschool.pokemoncards.data.models.pokemon.Resistance
import com.commschool.pokemoncards.data.models.pokemon.Weaknesse
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.lang.reflect.Type


private object Serializer {
    val instance: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    val abilityAdapter: JsonAdapter<List<Ability>> = instance.adapter<List<Ability>>(
        Types.newParameterizedType(
            List::class.java,
            Ability::class.java
        )
    )
    val attackAdapter: JsonAdapter<List<Attack>> = instance.adapter<List<Attack>>(
        Types.newParameterizedType(
            List::class.java,
            Attack::class.java
        )
    )
    val resistanceAdapter: JsonAdapter<List<Resistance>> = instance.adapter<List<Resistance>>(
        Types.newParameterizedType(
            List::class.java,
            Resistance::class.java
        )
    )
    val weaknesseAdapter: JsonAdapter<List<Weaknesse>> = instance.adapter<List<Weaknesse>>(
        Types.newParameterizedType(
            List::class.java,
            Weaknesse::class.java
        )
    )
}

class AbilityTypeConverter {

    @TypeConverter
    fun toString(ability: List<Ability>?): String? {
        return ability?.let { Serializer.abilityAdapter.toJson(it) }
    }

    @TypeConverter
    fun fromString(string: String?) = string?.let { Serializer.abilityAdapter.fromJson(it) }

}

class AttackTypeConverter {

    @TypeConverter
    fun toString(ability: List<Attack>?): String? {
        return ability?.let { Serializer.attackAdapter.toJson(it) }
    }

    @TypeConverter
    fun fromString(string: String?) = string?.let { Serializer.attackAdapter.fromJson(it) }

}

class ResistanceTypeConverter {

    @TypeConverter
    fun toString(ability: List<Resistance>?): String? {
        return ability?.let { Serializer.resistanceAdapter.toJson(it) }
    }

    @TypeConverter
    fun fromString(string: String?) = string?.let { Serializer.resistanceAdapter.fromJson(it) }

}

class WeaknesseTypeConverter {

    @TypeConverter
    fun toString(ability: List<Weaknesse>?): String? {
        return ability?.let { Serializer.weaknesseAdapter.toJson(it) }
    }

    @TypeConverter
    fun fromString(string: String?) = string?.let { Serializer.weaknesseAdapter.fromJson(it) }

}