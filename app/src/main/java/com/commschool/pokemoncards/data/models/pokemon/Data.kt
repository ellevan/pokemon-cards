package com.commschool.pokemoncards.data.models.pokemon


import com.squareup.moshi.Json
import androidx.annotation.Keep

@Keep
data class Data<T>(
    @Json(name = "data")
    val data: T
)