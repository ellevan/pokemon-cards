package com.commschool.pokemoncards.data.network

import com.commschool.pokemoncards.data.models.pokemon.Data
import com.commschool.pokemoncards.data.models.pokemon.PaginatedData
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {

    @GET("cards")
    suspend fun getCards(
        @Query("q", encoded = false) query: String? = null,
        @Query("page") page: Int? = null,
        @Query("pageSize") pageSize: Int? = null
    ): PaginatedData<List<PokeCard>>

    @GET("cards/{id}")
    suspend fun getCardBydId(
        @Path("id") id: String,
    ): Data<PokeCard>


}