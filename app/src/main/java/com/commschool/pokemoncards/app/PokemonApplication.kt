package com.commschool.pokemoncards.app

import android.app.Application
import android.content.Context
import com.commschool.pokemoncards.data.storage.DataStore

class PokemonApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DataStore.initialize(this, getSharedPreferences("_sp_", Context.MODE_PRIVATE))
    }


}