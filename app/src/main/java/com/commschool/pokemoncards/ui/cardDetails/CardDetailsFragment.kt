package com.commschool.pokemoncards.ui.cardDetails

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.commschool.pokemoncards.R
import com.commschool.pokemoncards.base.BaseFragment
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import com.commschool.pokemoncards.databinding.AttackItmBinding
import com.commschool.pokemoncards.databinding.CardDetailsScreenBinding
import com.commschool.pokemoncards.databinding.WeaknessItmBinding
import com.commschool.pokemoncards.ui.login.LoginViewModel
import com.commschool.pokemoncards.ui.set.SetFragmentDirections
import com.commschool.pokemoncards.utils.observeEvent

class CardDetailsFragment : BaseFragment() {

    private var binding: CardDetailsScreenBinding? = null

    private val cardDetailArg by navArgs<CardDetailsFragmentArgs>()

    private val viewModel by viewModels<CardDetailsViewModel> {
        CardDetailsViewModel.CardDetailsViewModelFactory(cardDetailArg.cardData)
    }

    private val loginViewModel by activityViewModels<LoginViewModel>()

    override fun getViewModelInstance() = viewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CardDetailsScreenBinding.inflate(inflater, container, false)
        return binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.cardModel.observe(viewLifecycleOwner) {
            showCardData(it)
        }
        binding?.backButton?.setOnClickListener {
            findNavController().popBackStack()
        }
        binding?.addRemoveBtn?.setOnClickListener {
            viewModel.buttonClicked()
        }
        viewModel.cardSaved.observe(viewLifecycleOwner) {
            when (it) {
                CardDetailsViewModel.CardSavedState.NotSaved -> binding?.addRemoveBtn?.setText(R.string.card_derails_add)
                CardDetailsViewModel.CardSavedState.Saved -> binding?.addRemoveBtn?.setText(R.string.card_derails_remove)
                else -> binding?.addRemoveBtn?.setText(R.string.card_derails_log_in)
            }
        }
        viewModel.loginRequired.observeEvent(viewLifecycleOwner) {
            activity?.findNavController(R.id.mainContainer)?.navigate(R.id.login)
        }
        loginViewModel.loginFlowFinished.observeEvent(viewLifecycleOwner) {
            if (it) viewModel.determineCardSavedState()
        }
        viewModel.showConfirmDialog.observeEvent(viewLifecycleOwner) {
            AlertDialog.Builder(requireContext()).setMessage(R.string.are_you_sure_delete)
                .setPositiveButton(
                    R.string.common_yes
                ) { dialog, which ->
                    dialog.dismiss();
                    viewModel.deleteConfirmed()
                }.setNeutralButton(R.string.common_no) { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }.show()
        }
        binding?.imgSet?.setOnClickListener {
            findNavController().navigate(
                CardDetailsFragmentDirections.formDetailsToSet(
                    cardDetailArg.cardData.set
                )
            )
        }
    }


    private fun showCardData(card: PokeCard) {
        binding?.apply {
            cardNameTV.text = card.name
            Glide.with(imgCard).load(card.images.large).placeholder(R.drawable.item_loader)
                .into(imgCard)
            cardTypeTV.text = card.types.joinToString(" ")
            Glide.with(imgSet).load(card.set.images.logo).placeholder(R.drawable.item_loader)
                .into(imgSet)
            setNameTV.text = card.set.name
            totalCardTV.text = getString(R.string.card_details_total_cards).format(card.set.total)
            seriesTV.text = getString(R.string.card_details_series).format(card.set.series)
            releaseDateTV.text =
                getString(R.string.card_details_release_date).format(card.set.releaseDate)

            hpBar.progress = card.hp.toInt()
            superTypeValueTv.text = card.supertype
            evolvesFromValueTv.text = card.evolvesFrom
            rarityValueTv.text = card.rarity
            showAttacks(card)
            showWeakness(card)
        }
    }


    private fun showAttacks(card: PokeCard) = binding?.apply {
        if (card.attacks.isEmpty()) {
            attacksContainer.visibility = View.GONE
            titleAttacks.visibility = View.GONE
        } else {
            attacksContainer.visibility = View.VISIBLE
            titleAttacks.visibility = View.VISIBLE
            attacksContainer.removeAllViews()
            card.attacks.map {
                val binding = AttackItmBinding.inflate(layoutInflater, attacksContainer, false)
                binding.attackName.text = it.name
                binding.costTv.text = it.cost.joinToString(",")
                binding.damnageTv.text =
                    getString(R.string.card_details_damange).format(it.damage)
                binding.description.text = it.text
                binding
            }.forEach {
                attacksContainer.addView(it.root)
            }
        }
    }


    private fun showWeakness(card: PokeCard) = binding?.apply {
        if (card.weaknesses.isEmpty()) {
            weaknessContainer.visibility = View.GONE
            weaknessTitle.visibility = View.GONE
        } else {
            weaknessContainer.visibility = View.VISIBLE
            weaknessTitle.visibility = View.VISIBLE
            weaknessContainer.removeAllViews()
            card.weaknesses.map {
                val binding = WeaknessItmBinding.inflate(layoutInflater, attacksContainer, false)
                binding.weaknessNameTv.text = it.type
                binding.weaknessCostTV.text = it.value
                binding
            }.forEach {
                weaknessContainer.addView(it.root)
            }
        }
    }

}