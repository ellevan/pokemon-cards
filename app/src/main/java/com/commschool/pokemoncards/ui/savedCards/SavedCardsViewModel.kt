package com.commschool.pokemoncards.ui.savedCards

import androidx.lifecycle.*
import com.commschool.pokemoncards.base.BaseViewModel
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import com.commschool.pokemoncards.data.repository.Repository
import com.commschool.pokemoncards.data.storage.db.entities.SavedCardIdEntity
import com.commschool.pokemoncards.utils.Event
import com.commschool.pokemoncards.utils.handleNetworkError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class SavedCardsViewModel : BaseViewModel() {

    private val _requestLogin = MutableLiveData<Event<Unit>>()
    val requestLogin: LiveData<Event<Unit>> get() = _requestLogin

    val userCards: LiveData<List<PokeCard>> =
        Repository.getLocalSavedCardsFlow()
            .map { list ->
                list.map { id ->
                    var card: PokeCard?
                    card = Repository.getLocalCardById(id)
                    if (card == null) {
                        showLoading()
                        card = Repository.getRemoteCardById(id)
                        hideLoading()
                    }
                    card
                }
            }.catch { error -> handleNetworkError(error) }
            .flowOn(Dispatchers.IO)
            .asLiveData(viewModelScope.coroutineContext)

    init {
        getSavedCards()
    }

    fun getSavedCards() = viewModelScope.launch(Dispatchers.IO) {
        try {
            if (!Repository.checkSavedIdsValidity()) {
                showLoading()
                Repository.updateRemoteSavedCards()
            }
        } catch (e: Exception) {
            handleNetworkError(e)
        } finally {
            hideLoading()
        }
    }

    fun refresh() = viewModelScope.launch(Dispatchers.IO) {
        try {
            showLoading()
            Repository.updateRemoteSavedCards()
        } catch (e: Exception) {
            handleNetworkError(e)
        } finally {
            hideLoading()
        }
        getSavedCards()
    }

    override fun onUnauthorized() {
        _requestLogin.postValue(Event(Unit))
    }

}