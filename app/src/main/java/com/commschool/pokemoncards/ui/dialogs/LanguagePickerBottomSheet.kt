package com.commschool.pokemoncards.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.commschool.pokemoncards.data.storage.DataStore
import com.commschool.pokemoncards.databinding.LanguageBottomSheetFragmentBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class LanguagePickerBottomSheet : BottomSheetDialogFragment() {

    lateinit var binding: LanguageBottomSheetFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LanguageBottomSheetFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.langButtonEng.setOnClickListener {
            DataStore.language = "en"
            dismiss()
            activity?.recreate()
        }
        binding.langButtonGeo.setOnClickListener {
            DataStore.language = "ka"
            dismiss()
            activity?.recreate()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
            .apply { (this as BottomSheetDialog).behavior.expandedOffset = 300 }
    }

}