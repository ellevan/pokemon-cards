package com.commschool.pokemoncards.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.*
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.commschool.pokemoncards.R
import com.commschool.pokemoncards.base.*
import com.commschool.pokemoncards.data.models.user.UserProfile
import com.commschool.pokemoncards.data.network.NetworkClient
import com.commschool.pokemoncards.data.network.UserService
import com.commschool.pokemoncards.data.storage.DataStore
import com.commschool.pokemoncards.databinding.ProfileScreenBinding
import com.commschool.pokemoncards.ui.dialogs.LanguagePickerBottomSheet
import com.commschool.pokemoncards.ui.login.LoginFragment
import com.commschool.pokemoncards.ui.login.LoginViewModel
import com.commschool.pokemoncards.utils.observeEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class ProfileFragment : BaseFragment() {

    private var binding: ProfileScreenBinding? = null

    private val viewModel by viewModels<ProfileViewModel>()

    private val loginViewModel by activityViewModels<LoginViewModel>()

    override fun getViewModelInstance() = viewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ProfileScreenBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.languagesButton?.setOnClickListener {
            val languagePickerBottomSheet = LanguagePickerBottomSheet()
            languagePickerBottomSheet.show(childFragmentManager, "tag")
        }
        binding?.logoutButton?.setOnClickListener {
            loginViewModel.logOut()
            findNavController().navigate(R.id.show_home)
        }

        viewModel.userProfile.observe(viewLifecycleOwner, this::showUserData)
        viewModel.loginRequired.observeEvent(viewLifecycleOwner) {
            loginViewModel.logOut()
            activity?.findNavController(R.id.mainContainer)?.navigate(R.id.login)
        }
        loginViewModel.loginFlowFinished.observeEvent(viewLifecycleOwner) { loginSuccess ->
            if (loginSuccess)
                viewModel.getProfile()
            else
                findNavController().navigate(R.id.show_home)
        }
    }

    private fun showUserData(userProfile: UserProfile) {
        binding?.userNameTextView?.text = userProfile.userName
        binding?.nameTextView?.text = userProfile.name
        Glide.with(this@ProfileFragment)
            .load(userProfile.imageUrl)
            .centerCrop()
            .into(binding?.profilePictureImageView!!)
    }
}