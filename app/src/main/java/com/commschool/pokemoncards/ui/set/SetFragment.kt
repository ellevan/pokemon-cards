package com.commschool.pokemoncards.ui.set

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.commschool.pokemoncards.R
import com.commschool.pokemoncards.base.BaseFragment
import com.commschool.pokemoncards.base.BaseViewModel
import com.commschool.pokemoncards.databinding.HomeScreenBinding
import com.commschool.pokemoncards.databinding.SetScreenBinding
import com.commschool.pokemoncards.ui.cardDetails.CardDetailsFragmentDirections
import com.commschool.pokemoncards.ui.home.CardAdapter
import com.commschool.pokemoncards.utils.LoadMoreListener
import com.commschool.pokemoncards.utils.PokemonCardDecorator

class SetFragment : BaseFragment() {

    private val setParam by navArgs<SetFragmentArgs>()
    private val viewModel: SetViewModel by viewModels { SetViewModelFactory(setParam.setData) }

    override fun getViewModelInstance() = viewModel

    private var binding: SetScreenBinding? = null

    private val adapter = CardAdapter {
        val action = CardDetailsFragmentDirections.actionGlobalCardDetailsFragment(it)
        activity?.findNavController(R.id.mainContainer)?.navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SetScreenBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = GridLayoutManager(context, 2)
        layoutManager.spanSizeLookup = CardAdapter.LoaderSpanSizeLookup(adapter)
        binding?.apply {
            recycleView.layoutManager = layoutManager
            recycleView.adapter = adapter
            recycleView.addItemDecoration(
                PokemonCardDecorator(
                    itemHorizontalInsets = resources.getDimensionPixelSize(R.dimen.card_list_hor_inst),
                    itemHorizontalSpacing = resources.getDimensionPixelSize(R.dimen.card_list_hor_space),
                    itemVerticalInsets = resources.getDimensionPixelSize(R.dimen.card_list_ver_inst),
                    itemVerticalSpacing = resources.getDimensionPixelSize(R.dimen.card_list_vrt_space),
                )
            )
        }

        viewModel.items.observe(viewLifecycleOwner) {
            adapter.cardList = it
        }
        viewModel.setName.observe(viewLifecycleOwner) {
            binding?.setNameTV?.text = it
        }
        binding?.backButton?.setOnClickListener {
            findNavController().popBackStack()
        }
    }


}