package com.commschool.pokemoncards.ui.main

class MainActivity : com.commschool.pokemoncards.base.LanguageAwareActivity() {

    private val binding: com.commschool.pokemoncards.databinding.MainActivityBinding by lazy {
        com.commschool.pokemoncards.databinding.MainActivityBinding.inflate(
            layoutInflater
        )
    }

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

}