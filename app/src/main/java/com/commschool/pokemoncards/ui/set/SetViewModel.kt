package com.commschool.pokemoncards.ui.set

import androidx.lifecycle.*
import com.commschool.pokemoncards.base.BaseViewModel
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import com.commschool.pokemoncards.data.models.pokemon.Set
import com.commschool.pokemoncards.data.repository.Repository
import com.commschool.pokemoncards.utils.handleNetworkError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.handleCoroutineException
import kotlinx.coroutines.launch
import java.lang.Exception

class SetViewModel(private val data: Set) : BaseViewModel() {

    private val _items = MutableLiveData<List<PokeCard>>()
    val items: LiveData<List<PokeCard>> get() = _items

    private val _setName = MutableLiveData<String>(data.name)
    val setName: LiveData<String> get() = _setName

    init {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                showLoading()
                _items.postValue(Repository.getCardsBySet(data.id))
            } catch (e: Exception) {
                handleNetworkError(e)
            } finally {
                hideLoading()
            }
        }
    }
}

class SetViewModelFactory(private val data: Set) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SetViewModel(data) as T
    }
}