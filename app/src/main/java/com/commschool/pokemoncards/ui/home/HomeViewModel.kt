package com.commschool.pokemoncards.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.commschool.pokemoncards.R
import com.commschool.pokemoncards.base.BaseViewModel
import com.commschool.pokemoncards.base.DialogData
import com.commschool.pokemoncards.data.models.pokemon.PokeCard
import com.commschool.pokemoncards.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel : BaseViewModel() {

    private val _items = MutableLiveData<List<PokeCard>>()
    val items: LiveData<List<PokeCard>> get() = _items

    private val _loadingMore = MutableLiveData(false)
    val loadingMore: LiveData<Boolean> get() = _loadingMore

    private var noMoreData = false

    private var page = 1

    init {
        loadMore()
    }

    fun onScrollEndReached() {
        if (loadingMore.value == true || noMoreData) return
        loadMore()
    }

    fun onRefresh() {
        page = 1
        _items.postValue(emptyList())
        loadMore()
    }

    private fun loadMore() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _loadingMore.postValue(true)
                val cards = Repository.getRemoteCardsCardsAndStore(
                    page = page,
                    pageSize = PAGE_SIZE
                )
                noMoreData = cards.count == cards.totalCount
                page++
                _items.postValue((_items.value ?: emptyList()) + cards.data)
            } catch (e: Exception) {
                showDialog(DialogData(title = R.string.common_error, message = e.message ?: ""))
            } finally {
                _loadingMore.postValue(false)
            }
        }
    }

    companion object {
        const val PAGE_SIZE = 20
    }
}