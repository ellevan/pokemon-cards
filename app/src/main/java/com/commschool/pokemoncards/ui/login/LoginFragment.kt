package com.commschool.pokemoncards.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.*
import androidx.navigation.fragment.findNavController
import com.commschool.pokemoncards.R
import com.commschool.pokemoncards.base.*
import com.commschool.pokemoncards.databinding.LoginScreenBinding
import com.commschool.pokemoncards.ui.registration.RegistrationFragment
import com.commschool.pokemoncards.utils.observeEvent

class LoginFragment : BaseFragment(), View.OnClickListener {

    private var binding: LoginScreenBinding? = null

    private val viewModel: LoginViewModel by activityViewModels()

    override fun getViewModelInstance() = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(RegistrationFragment.KEY_DATA) { _, bundle ->
            binding?.userNameInput?.setText(bundle.getString(RegistrationFragment.KEY_USERNAME))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LoginScreenBinding.inflate(inflater, container, false)
        return binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.registrationButton?.setOnClickListener(this)
        binding?.emailSupportButton?.setOnClickListener(this)
        binding?.loginButton?.setOnClickListener(this)
        binding?.backButton?.setOnClickListener(this)
        viewModel.inputError.observe(viewLifecycleOwner) {
            binding?.passwordInput?.error = getString(it)
        }
        viewModel.loginSuccess.observeEvent(viewLifecycleOwner) {
            findNavController().popBackStack()
        }
        viewModel.loginFragmentStarted()
    }


    override fun onClick(v: View?) {
        hideKeyboard()
        when (v) {
            binding?.registrationButton -> {
                startRegistration()
            }
            binding?.backButton -> {
                findNavController().popBackStack()
            }
            binding?.emailSupportButton -> {
                sendEmailToSupport()
            }
            binding?.loginButton -> {
                viewModel.login(
                    username = binding?.userNameInput?.text,
                    password = binding?.passwordInput?.text
                )
            }
        }
    }

    override fun onDestroy() {
        viewModel.loginFragmentDestroyed()
        super.onDestroy()
    }

    private fun sendEmailToSupport() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_EMAIL, "bacho.kuratnidze@wandio.com")
        intent.putExtra(Intent.EXTRA_SUBJECT, "Hi Bachooo")
        if (intent.resolveActivity(context?.packageManager!!) != null) {
            startActivity(intent)
        }
    }

    private fun startRegistration() {
        findNavController().navigate(R.id.form_login_to_registration)
    }


    companion object {
        const val KEY_LOGIN_RESULT = "key_login_result"
        const val KEY_LOGIN_RESULT_SUCCESS = "key_login_result_success"
    }
}